//Zeige Box nur mit Javascript an
document.getElementById("box").style.display = "block";
/*

  Nachricht von Thilaksan an die Designer.

  Nur die Funktion "addTermin" ist für euch wichtig,
  alles andere könnt ihr hier überspringen.

*/

function addTermin(table,date,text,append=true) {
  var table = document.getElementById(table).querySelector("table");
  //haben wir schon 3 Termine?
  if(table.querySelectorAll("tr").length>3)
    return false;

  //clone item,mache es sichtbar
  var tr = table.querySelector("tr").cloneNode(true);
  tr.style.visibility = "visible";
  //füge es hinzu
  if(append)
    table.appendChild(tr);
  else
    table.prepend(tr);

  //bestück es mit informationen
  tr.querySelectorAll("td")[0].innerHTML = date;
  tr.querySelectorAll("a")[0].innerHTML = "<b>"+text+"</b>";
  return true;
}


//CRAWL CODE - unintressant für  Designer.

function sendRequest() {
  //AJAX
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'Script_Kalender.php');
  xhr.responseType = 'document';
  xhr.onload = function () {
    if (xhr.readyState === xhr.DONE && xhr.status === 200) {
      
      processXML_Maphin(xhr.responseXML);
      processXML_Physik(xhr.responseXML);
      processXML_Informatik(xhr.responseXML);
      processXML_Mathematik(xhr.responseXML);
    }
  }
  xhr.send(null);  
}
function processXML_Maphin(xml) {
  var allnames = xml.querySelectorAll("#main")[0].querySelectorAll(".name");
  console.log(allnames);
    for (var i = 0; i < allnames.length; i++) {
      processNode_Maphin(allnames[i]);
    }
}
function processXML_Informatik(xml) {
  //INFORMATIK
  var parent = xml.getElementsByClassName("mw-parser-output")[0];
  var all = parent.childNodes;

  var current = null;
  var weneedmore = true;
  for (var i = 0; i < all.length&&weneedmore; i++) {
    if(all[i].tagName?(all[i].tagName=="EmptyTextNode"):1){
      parent.removeChild(all[i]);
      i--;
    }
    if(all[i].tagName == "H3"){
      if(current?current!==all[i]:0)
        if(!processNode_Informatik(current))
          weneedmore = false;
      current = all[i];
    }else if(current){
      try {
        current.appendChild(all[i]);
        parent.removeChild(all[i]);
        i--;
      } catch (e) {

      }
    }
  }
}
function processXML_Mathematik(xml) {
  var date = new Date;
  var today = date.getDate();

  var body = xml.querySelector("#terminkalender>tbody");
  var startRecord = false; //When should we start recording?
  var month = date.getMonth();
  var year = date.getFullYear();
  var nextMonth = false;
  var nextYear = false;
  
  body.querySelectorAll("td").forEach(td => { 
    var day = td.firstElementChild.innerText;
    if(day>=today) //nach dem aktuellsten Tag,alles aufnehmen.
      startRecord=true;

    if(startRecord){
      if(day<today) //nächster Monat
        nextMonth = true;
      if(month+nextMonth>=12)//nächstes Jahr
        nextYear = true;
      
      var termin = td.querySelectorAll("a");
      if(termin.length>0){
        if(!addTermin("termin_Mathe",day+"."+(month+nextMonth+1)+"."+(year+nextYear),termin[0].innerText,false))
        return;
      }
    }
  });
}
function processXML_Physik(xml) {
  var weneedmore = true;
  //PHYSIK
  var alltrs = xml.getElementsByClassName("eventlist")[0].querySelectorAll("tr");
  for (var i = 0; i < alltrs.length&&weneedmore; i++) {
    if(!processNode_Physik(alltrs[i]))
      weneedmore = false;
  }

}

function processNode_Physik(o) {
  var tds = o.querySelectorAll("td");

  var date = tds[0].textContent;
  var title = tds[1].querySelector("b").textContent;

  return addTermin("termin_Physik",date,title,false);
}
function processNode_Informatik(o){
  var title = o.querySelector(".mw-headline").textContent;
  var date = "";
  if(title.search("am")!==-1){
    date = title.substr(title.search("am")+2);
    title = title.substr(0,title.search("am"));
  }
  var more = o;

  return addTermin("termin_Informatik",date,title);
}
function processNode_Maphin(o){
  console.log(o);
  var date = o.querySelectorAll(".date");
  var a = o.querySelectorAll("a");
  console.log(a);
  if(date.length<1)
    return false;
  return addTermin("termin_Maphin",date[0].textContent,a[0].textContent);
}

sendRequest();