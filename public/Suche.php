<?php 
if(!isset($_GET["search"])){
    header("Location: index.php");
    exit;
}

$CUSTOM=true;
$PAGE = 'Suche'; 
include "Script_Search.php";
include "Script_Template.php";


function generateCustomPosts($page='Suche'){
    $posts = searchPosts();
    foreach ($posts as $obj) {
        if($obj->formatting==="html")
          generatePost($obj);
        if($obj->formatting==="link")
          generatePostLink($obj);
        if($obj->formatting==="bbcode")
        ;//florian want to make?Question makr.
      }
}

//gibt zurück ob zwei wörter ähnlich sind
function similar($a,$b)
{
    $count_correct = 0;
    $i=0;
    //anfang vergleichen
    for ( ;$i < strlen($a)&&$i < strlen($b); $i++) { 
        if($a[$i]==$b[$i])
            $count_correct++;
    }
    if($i==0)
        return false;

    //letzten zeichen vergleichen
    for ($j = 0 ;$j < min(strlen($a),strlen($b)) ; $j++) {
        if($a[strlen($a)-1-$j]==$b[strlen($b)-1-$j])
            $count_correct++;
    }
    //bei mehr als 80% ähnlichkeit
    return ($count_correct/(min(strlen($a),strlen($b))))>0.8;
}
//gibt Array mit den Post wieder
function searchPosts()
{
    $posts = json_decode(file_get_contents ("content/cache/suche.json"))->posts;
    //wörter aus der Suchanfrage
    $words =  explode(" ",preg_replace("![^a-z0-9]+!i"," ",strtolower(strip_tags ($_GET["search"]))));
    
    $output_postList = [];    
    foreach ($posts as $post) {
        $count_correct = 0;
        //titel haben doppelte gewichtung
        foreach ($post->keywordtitle as $titleword) {
            foreach ($words as $word) {
                if(similar($titleword,$word))
                    $count_correct+=2;
            }
        }
        //keywords haben normale gewichtung
        foreach ($post->keywords as $keyword) {
            foreach ($words as $word) {
                if(similar($keyword,$word))
                    $count_correct++;
            }
        }
        //normalisieren von 0 bis 1 
        $post->weight = $count_correct/(count($post->keywords)+2*count($post->keywordtitle));
        //weight muss über einem Wert liegen
        if($post->weight>0)
            array_push($output_postList,$post);
    }
    //sortiert
    usort($output_postList,function($a,$b)
    {
        if($a->weight==$b->weight)
            return 0;
        return ($a->weight>$b->weight)?-1:1;
    });
    //weight attribute nicht mehr benötigt
    foreach ($output_postList as $post) {
        $post->weight = null;
    }
    return $output_postList;
}


?>
