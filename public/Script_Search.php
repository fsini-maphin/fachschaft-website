<?php
//Cacht einmal pro Tag potenzielle Keywords
/*
    Jedem Post werden Keywords zugeordnet.
    ======================================
    ||  Je häufiger ein Wort in einem Artikel und je seltener es im allgemeinen Vorkommt,desto höher ist die gewichtung.
    ||  Die 10 Wörter mit der Höhsten gewichtung gelten als Keywords.
    
    Ausgabe format von Posts: {"file":_,"title":_,"link":_"keywords":[_],"keywordtitle":[_]}
        file:       Website link
        title:      Titel
        Link:       link zum Post im Content Ordner
        Keywords:   Keywords array
        KeywordTitle:   Title als keyword
*/
new CacheGenerator;

class CacheGenerator{
    //zählt die häufigkeit von wörtern im generellen.
    //Anzahl der Posts
    private $wordCount = array();
    private $postCount = 0;

    function __construct() {
        //already updated for today?
        if(json_decode(file_get_contents("./content/cache/suche.json"))->lastUpdate==getDate()["mday"])
            return;

        //Sammelt alle Post.
        $postList = $this->postListAllFiles();
        //Verarbeitet die Posts und ezeugt eine cache/suche.json
        $this->analysePostList($postList);
    }
    /*
        Erstmal alle Dateien auslesen um ihre Posts zu sammeln.
        Returnt eine Liste alle Posts. Im Format:
            link=>post
    */
    function postListAllFiles()
    {
        //Das array,das die ReturnValue speichert.
        $output_allPosts = array();
        
        //alle datein durchgehen
        $array = scandir ( "./content/", SCANDIR_SORT_ASCENDING );
        foreach($array as $file){
            $name = pathinfo($file)["filename"];
            $extension = isset(pathinfo($file)["extension"])?pathinfo($file)["extension"]:"";

            if($extension==='json'&&!str_contains($name,"Script_")){
                
                //alle Posts durchgehen
                $postsInFile = json_decode(file_get_contents ("content/".$name.".json"));
                $output_postsInFile = [];
                foreach($postsInFile as &$post){
                    //unakzeptable:
                    if($post->formatting!="html")
                        break;
        
                    //speichert das Vorkommen des Post in der letzten Datei
                    $post->file = $name.".php";

                    $output_postsInFile[$post->link]=$post;
                }
                $output_allPosts = array_merge($output_allPosts,$output_postsInFile);
            }
        }
        return $output_allPosts;
    }
    
    /*
        Analysiert Array das aus Post besteht.
        aktualisiert content/cache/suche.json
    */
    function analysePostList($posts){
        //speicher anzahl der posts
        $this->postCount = count($posts);

        //analysiere Post
        foreach ($posts as &$obj) {
            $obj = $this->analysePost($obj);
        }
        //optimiere die Keywords
        foreach ($posts as &$obj) {
            $this->sortKeyword($obj,10);
        }
        //schreibe alles auf.
        file_put_contents("./content/cache/suche.json",json_encode((object)[
            "lastUpdate"=>getDate()["mday"],
            "posts"=>$posts
        ]));
    }
    /*
    
        analysiert einen Post.
        return ein Post mit zusatzinfomationen "file":_,keywords":[_],"keywordtitle":[_]
    */
    function analysePost($post)
    {
        //lese content und entferne unnötiges
        $content = strip_tags (file_get_contents("content/content/".$post->link))." ".$post->file;
        $content =  preg_replace("![^a-z0-9]+!i"," ",strtolower($content));
        
        //füge wörter zur allgemeinen häufigkeit hinzu 
        $content_words = explode(" ",$content);
        foreach($content_words as $word)
            $this->countWords($word);

        //lese titel  und entferne unnötiges
        $title =  preg_replace("![^a-z0-9]+!i"," ",strtolower(strip_tags($post->title)));
        $title_words = explode(" ",$title);
        foreach($title_words as $word)
            $this->countWords($word);

        $post->keywords = $content_words;
        $post->keywordtitle= $title_words;
        return $post;
    }
    //updated den Wordcount
    function countWords($word)
    {
        if(!isset($this->wordCount[$word]))
            $this->wordCount[$word] = 0;
        else $this->wordCount[$word]+=1;
        
    }
    //Nur die top $top keywords bleiben besteheen. Der rest wird entfernt.
    function sortKeyword(&$post,$top)
    {
        $output_keywords = [];
        $counts = array_count_values($post->keywords);
        foreach ($counts as $key => $value) {
            $weight = $value-(($this->wordCount[$key])/$this->postCount);
            if(0<$weight){
                $output_keywords[$key]=$weight;
            }
        }
        arsort($output_keywords);
        $post->keywords = [];
        foreach ($output_keywords as $key => $value) {
            if($top==0)
            return;
            array_push($post->keywords,strval($key));    
            $top--;
        }
    }
};
?>