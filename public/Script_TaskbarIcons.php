<?php
function generateTaskbarIcon($name='',$icon='',$link='')
{
	echo '
	<a href="'.$link.'" id="taskbar-icon-'.$name.'" title="'.$name.'">
		'.generateImage($name,$icon).'<br>
		'.$name.'
	</a>';
}
function generateImage($name,$icon)
{
	if (substr_compare($icon,".svg",-4)===0){
		return file_get_contents($icon);
	}else{
		return '<img src="'.$icon.'" alt="'.$name.'" >';
	}
}
generateTaskbarIcon("Fachschaft","img/muffin.svg","Fachschaft.php");
generateTaskbarIcon("Termine & Events","img/kalender.svg","Termine.php");
generateTaskbarIcon("Freizeit","img/freizeit.svg","Freizeit.php");
generateTaskbarIcon("Erstis","img/ersti.svg","index.php#what_is_IMP");
generateTaskbarIcon("Studium","img/studium.svg","Studium.php");
generateTaskbarIcon("AGNES","img/zahnrad.svg","https://agnes.hu-berlin.de/lupo/rds?state=user&type=0");
generateTaskbarIcon("Moodle","img/moodle.svg","https://moodle.hu-berlin.de");
generateTaskbarIcon("Fragen","img/fragen.svg","Fragen.php");
generateTaskbarIcon("<b>Imp</b>ressum","img/info.svg","Impressum.php");
?>