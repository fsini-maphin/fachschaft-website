<!DOCTYPE html>
<html lang="de">
	<head>
	<test></test>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>IMP - <?php echo $PAGE; ?></title>

		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" type="image/png" href="favicon_32px.png" sizes="32x32">
		<link rel="icon" type="image/png" href="favicon_96px.png" sizes="96x96">
		<link rel="apple-touch-icon" sizes="180x180" href="favicon_180px.png">

		<link rel="stylesheet" href="css/template.css">
	</head>
	<body>
		<div id="grid-container">
			<div id="nav-container">
				<div id="nav">
					<?php include 'Script_TaskbarIcons.php'; ?>
				</div>
			</div>
			<div id="menu-container">
				<div id="menu">
					<h1>Menu</h1>
					<form action="/Suche.php">
						<input name="search" type="text" placeholder="Search...">
					</form>
					<?php include "Script_Menu.php"; ?>
				</div>
			</div>
			<div id="main-container">
				<div id="main">
					<h1 id="main-title">
						<?php echo $PAGE; ?>
					</h1>
					<?php
						if(isset($CUSTOM)?$CUSTOM:false){
							//eine php erstellte version laden.
							generateCustomPosts($PAGE);
						}else {
							//Aus JSON Dateien geladen.
							generatePosts($PAGE);
						}
					?>
				</div>
			</div>
			<div id="news-container">
				<div id="news">
					<h1>News</h1>
					<?php include "Script_News.php"; ?>
				</div>
			</div>
		</div>
	</body>
</html>

<?php
  function generatePosts($page){
    $posts = json_decode(file_get_contents ("content/".$page.".json"));

    foreach ($posts as $obj) {
      if($obj->formatting==="html")
        generatePost($obj);
      if($obj->formatting==="link")
        generatePostLink($obj);
      if($obj->formatting==="markdown")
      	generatePostMarkdown($obj);
    }
  }
  function generatePost($meta){
    echo '
    <div class="event">
      <div class="name">
		<a'.(isset($meta->file)?" href='$meta->file'":"")." >"
		.$meta->title.'</a>
        <div class="date">'
          .$meta->date.
        '</div>
      </div>
      <hr>
      '.file_get_contents("content/content/".$meta->link).'
    </div>';
  }
  function generatePostLink($meta){
    echo '<div class="event">
      <div class="name">
        <a href="'.$meta->link.'" style="color:black;text-decoration: none;">&#9658;'.$meta->title.'</a>
        <!--&#9660;-->
      </div>
    </div>';
  }


  function generatePostMarkdown($meta){
	include "libs\parsedown\Parsedown.php";

	$Parsedown = new Parsedown();
	$Parsedown->setSafeMode(true);
	
    echo '
    <div class="event">
      <div class="name">
		<a'.(isset($meta->file)?" href='$meta->file'":"")." >"
		.$meta->title.'</a>
        <div class="date">'
          .$meta->date.
        '</div>
      </div>
      <hr>
      '.$Parsedown->text(file_get_contents("content/content/".$meta->link)).'
    </div>';
  }
?>